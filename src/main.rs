use std::fmt;

const N: usize = 14;

#[derive(Clone)]
enum Blocked {
    Unnumbered,
    Numbered { cur: usize, max: usize },
}

#[derive(Clone)]
enum Square {
    Free,
    Lit,
    Lamp,
    Blocked(Blocked),
}

impl fmt::Display for Square {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Free => write!(f, "."),
            Square::Lit => write!(f, "-"),
            Square::Lamp => write!(f, "o"),
            Square::Blocked(blocked) => {
                match blocked {
                    Blocked::Unnumbered => write!(f, "#"),
                    Blocked::Numbered { cur: _, max } => write!(f, "{}", max.to_string()),
                }
            }
        }
    }
}

enum LightErr {
    Blocked,
    Overflow,
    Clash,
}

#[derive(Clone)]
struct Board(Vec<Vec<Square>>);

impl Board {
    fn new(squares: Vec<Vec<Square>>) -> Board {
        assert_eq!(squares.len(), N);
        for row in &squares {
            assert_eq!(row.len(), N);
        }
        Board(squares)
    }

    fn neighbor_mut(&mut self, x: usize, y: usize, i: usize) -> Option<&mut Square> {
        if x > 0 && i == 0 {
            return Some(&mut self.0[x-1][y])
        }
        if x < N - 1 && i == 1 {
            return Some(&mut self.0[x+1][y])
        }
        if y > 0 && i == 2 {
            return Some(&mut self.0[x][y-1])
        }
        if y < N - 1 && i == 3 {
            return Some(&mut self.0[x][y+1])
        }
        None
    }

    fn neighbor(&self, x: usize, y: usize, i: usize) -> Option<&Square> {
        if x > 0 && i == 0 {
            return Some(&self.0[x-1][y])
        }
        if x < N - 1 && i == 1 {
            return Some(&self.0[x+1][y])
        }
        if y > 0 && i == 2 {
            return Some(&self.0[x][y-1])
        }
        if y < N - 1 && i == 3 {
            return Some(&self.0[x][y+1])
        }
        None
    }

    fn lights_clash(&self, x: usize, y: usize) -> bool {
        for i in (0..x).rev() {
            match self.0[i][y] {
                Square::Lamp => return true,
                Square::Blocked(_) => break,
                Square::Free => {},
                Square::Lit => {},
            }
        }
        for i in (x+1)..N {
            match self.0[i][y] {
                Square::Lamp => return true,
                Square::Blocked(_) => break,
                Square::Free => {},
                Square::Lit => {},
            }
        }
        for j in (0..y).rev() {
            match self.0[x][j] {
                Square::Lamp => return true,
                Square::Blocked(_) => break,
                Square::Free => {},
                Square::Lit => {},
            }
        }
        for j in (y+1)..N {
            match self.0[x][j] {
                Square::Lamp => return true,
                Square::Blocked(_) => break,
                Square::Free => {},
                Square::Lit => {},
            }
        }
        return false
    }

    fn overflows_neighbor(&self, x: usize, y: usize) -> bool {
        for i in 0..=4 {
            let neighbor = self.neighbor(x, y, i);
            if let Some(Square::Blocked(Blocked::Numbered { cur, max })) = neighbor {
                if *cur + 1 > *max {
                    return true
                }
            }
        }
        false
    }

    fn light_up(&self, x: usize, y: usize) -> Result<Board, LightErr> {
        if let Square::Blocked(_) = self.0[x][y] {
            return Err(LightErr::Blocked)
        }

        if self.overflows_neighbor(x, y) {
            return Err(LightErr::Overflow)
        }

        if self.lights_clash(x, y) {
            return Err(LightErr::Clash)
        }

        // valid square
        let mut new_board = self.clone();
        new_board.0[x][y] = Square::Lamp;

        for i in 0..=4 {
            let neighbor = new_board.neighbor_mut(x, y, i);
            if let Some(Square::Blocked(Blocked::Numbered { cur, max: _ })) = neighbor {
                *cur += 1;
            }
        }

        for i in (0..x).rev() {
            match new_board.0[i][y] {
                Square::Lamp => panic!(),
                Square::Blocked(_) => break,
                Square::Free => new_board.0[i][y] = Square::Lit,
                Square::Lit => {},
            }
        }
        for i in (x+1)..N {
            match new_board.0[i][y] {
                Square::Lamp => panic!(),
                Square::Blocked(_) => break,
                Square::Free => new_board.0[i][y] = Square::Lit,
                Square::Lit => {},
            }
        }
        for j in (0..y).rev() {
            match new_board.0[x][j] {
                Square::Lamp => panic!(),
                Square::Blocked(_) => break,
                Square::Free => new_board.0[x][j] = Square::Lit,
                Square::Lit => {},
            }
        }
        for j in (y+1)..N {
            match new_board.0[x][j] {
                Square::Lamp => panic!(),
                Square::Blocked(_) => break,
                Square::Free => new_board.0[x][j] = Square::Lit,
                Square::Lit => {},
            }
        }
        Ok(new_board)
    }

    fn has_free(&self) -> bool {
        for row in &self.0 {
            for square in row {
                if let Square::Free = square {
                    return true
                }
            }
        }
        false
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut res = String::new();
        for row in &self.0 {
            for square in row {
                res.push_str(&square.to_string());
            }
            res.push('\n');
        }
        write!(f, "{}", res)
    }
}

fn main() {
    let mut board = Board::new(vec![
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Blocked(Blocked::Unnumbered), Square::Free, Square::Blocked(Blocked::Unnumbered), Square::Free, Square::Blocked(Blocked::Unnumbered), Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Blocked(Blocked::Numbered{ cur: 0, max: 3 }), Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Blocked(Blocked::Unnumbered), Square::Blocked(Blocked::Numbered{ cur: 0, max: 1 }), Square::Free, Square::Free, Square::Free, Square::Blocked(Blocked::Numbered{ cur: 0, max: 1 }), Square::Blocked(Blocked::Unnumbered)],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free],
        vec![Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free, Square::Free]
    ]);
    println!("{}", board);

    let mut i = 0;
    let mut j = 0;
    while board.has_free() {
        board = match board.light_up(i, j) {
            Ok(new_board) => new_board,
            Err(_) => board,
        };

        if i < N - 1 {
            i += 1;
        } else {
            i = 0;
            if j < N - 1 {
                j += 1;
            } else {
                j = 0;
            }
        }
    }

    println!("{}", board);
}
